package uz.pdp.bookreviewservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.bookreviewservice.model.StatusEnum;

import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.sql.Timestamp;
import java.util.UUID;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class BookRev {
    private UUID userId;
    private String review;
    private short rate;
    private String status;
}
