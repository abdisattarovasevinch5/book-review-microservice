package uz.pdp.bookreviewservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.clients.book.BookDto;

import java.util.List;

// Bahodir Hasanov 4/24/2022 8:44 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookReviewDto {
    private BookDto bookDto;
    private List<BookRev> bookReviews;
}
