package uz.pdp.bookreviewservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bookreviewservice.dto.BookRev;
import uz.pdp.bookreviewservice.dto.BookReviewDto;
import uz.pdp.bookreviewservice.model.BookReview;
import uz.pdp.bookreviewservice.repository.BookReviewRepository;
import uz.pdp.clients.book.BookClient;
import uz.pdp.clients.book.BookDto;
import uz.pdp.clients.email.Email;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class BookReviewService {

    private final BookReviewRepository bookReviewRepository;
    private final BookClient bookClient;
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;
    @Value("${spring.rabbitmq.routingkey}")
    private String routingkey;


    public void send(Email email) {
        rabbitTemplate.convertAndSend(exchange, routingkey, email);
    }


    public ResponseEntity<?> getAllBookReview(int page, int size, String search, String sort, boolean direction) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size,
                direction ? Sort.Direction.ASC : Sort.Direction.DESC,
                sort
        );
        Page<BookReview> bookReviews = bookReviewRepository.findAllReviewByPage(pageable, search);
        return null;
    }


    public ResponseEntity<?> getReviewByBookId(UUID bookId) {
        List<BookReview> allByBookReview = bookReviewRepository.findAllByBookId(bookId);
        BookDto bookDto = bookClient.getBookById(bookId);
        BookReviewDto bookReviewDto = new BookReviewDto();
        List<BookRev> bookRevs = new ArrayList<>();
        for (BookReview bookReview : allByBookReview) {
            BookRev bookRev = new BookRev();
            bookRev.setUserId(bookReview.getUserId());
            bookRev.setStatus(bookReview.getStatus().getStatus().toString());
            bookRev.setReview(bookReview.getReview());
            bookRevs.add(bookRev);
        }
        bookReviewDto.setBookReviews(bookRevs);
        bookReviewDto.setBookDto(bookDto);
        return ResponseEntity.ok(bookReviewDto);
    }


    public ResponseEntity<?> getAverageRate(UUID bookId) {
        Double average = bookReviewRepository.getAverage(bookId);
        return ResponseEntity.ok(average);

    }


}
