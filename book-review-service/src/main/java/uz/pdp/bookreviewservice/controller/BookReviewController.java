package uz.pdp.bookreviewservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bookreviewservice.service.BookReviewService;
import uz.pdp.clients.email.Email;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/book-review")
public class BookReviewController {
    private final BookReviewService bookReviewService;

    @Value("${app.message}")
    private String message;

    @PostMapping
    public String postReviewToABook(@RequestBody Email email) {
        bookReviewService.send(email);
        return message;
    }

    @GetMapping
    public ResponseEntity<?> getAllBookReview(
            @RequestParam(name = "size", defaultValue = "5") int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search,
            @RequestParam(name = "sort", defaultValue = "name") String sort
    ) {
        return bookReviewService.getAllBookReview(page, size, search, sort, true);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<?> saveBookReview(@PathVariable UUID bookId) {
        return bookReviewService.getReviewByBookId(bookId);
    }

    @GetMapping("/average-rating/{bookId}")
    public ResponseEntity<?> getAverageRate(@PathVariable UUID bookId) {
        return bookReviewService.getAverageRate(bookId);
    }
}








