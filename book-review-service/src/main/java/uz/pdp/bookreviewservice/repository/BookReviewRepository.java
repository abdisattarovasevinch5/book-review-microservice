package uz.pdp.bookreviewservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.bookreviewservice.model.BookReview;

import java.util.List;
import java.util.UUID;

@Repository
public interface BookReviewRepository extends JpaRepository<BookReview, UUID> {

    @Query(nativeQuery = true, value = "select * from book_reviews")
    Page<BookReview> findAllReviewByPage(Pageable pageable, String search);

    List<BookReview> findAllByBookId(UUID bookId);


    @Query(nativeQuery = true, value = "select   avg(br.rate) as rate\n" +
            "from book_reviews br  where book_id = :bookId")
    Double getAverage(UUID bookId);

}

