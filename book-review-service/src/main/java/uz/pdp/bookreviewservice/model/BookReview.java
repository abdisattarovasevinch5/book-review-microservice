package uz.pdp.bookreviewservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.bookreviewservice.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "book_reviews")
//import static uz.sardor.Main.*;
//Sardor {4/21/2022}{ 11:49 AM}
public class BookReview extends AbsEntity {

    private UUID userId;
    private UUID bookId;
    private short rate;
    private String review;
    @ManyToOne
    private StatusEnum status;

}
