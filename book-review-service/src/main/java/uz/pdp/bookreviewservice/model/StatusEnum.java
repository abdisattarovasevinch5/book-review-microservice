package uz.pdp.bookreviewservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.bookreviewservice.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "statuses")
//import static uz.sardor.Main.*;
//Sardor {4/21/2022}{ 11:56 AM}
public class StatusEnum extends AbsEntity {
    @Enumerated(EnumType.STRING)
    private Status status;
}
