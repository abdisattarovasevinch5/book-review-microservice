package uz.pdp.bookreviewservice.model;

public enum Status {
    NEW,
    ACCEPTED,
    REJECTED
}
