package uz.pdp.emailservice.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uz.pdp.clients.email.Email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


@RequiredArgsConstructor
@Component
public class MailService implements RabbitListenerConfigurer {


    private static final Logger logger = LoggerFactory.getLogger(MailService.class);

    @Value("${PERSONAL}")
    String personal;

    private final JavaMailSender javaMailSender;

    private final TemplateEngine templateEngine;
    LinkedList linkedList;

    @Override
    public void configureRabbitListeners(
            RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}")

    public void receivedMessage(Email email) {

        logger.info("Email received is.. " + email);

        sendEmailToModerator(email);

    }


    public void sendEmailToModerator(Email email) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            Context context = new Context();
            Map<String, Object> props = new HashMap<>();


            props.put("senderFullName", email.getSenderFullName());
            props.put("senderId", email.getSenderId());
            props.put("review", email.getReview());
            props.put("fullName", email.getReceiverFullName());
            props.put("bookId", email.getBookId());
            props.put("bookTitle", email.getBookTitle());
            props.put("acceptLink", email.getAcceptLink());
            props.put("rejectLink", email.getRejectLink());


            context.setVariables(props);

            helper.setFrom(personal);
            helper.setTo(email.getTo());
            helper.setSubject("New review");

            String html = templateEngine.process("email.html", context);
            helper.setText(html, true);

            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
