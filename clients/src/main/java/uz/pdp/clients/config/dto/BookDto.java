package uz.pdp.clients.config.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

//import javax.persistence.OneToOne;
import java.sql.Timestamp;
import java.util.UUID;

@NoArgsConstructor
@Data
@AllArgsConstructor
@PackagePrivate
public class BookDto {

    UUID id;
    Timestamp createdAt;
    String tittle;
    String description;
    UUID attachmentId;


}
