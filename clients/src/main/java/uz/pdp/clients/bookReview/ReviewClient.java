package uz.pdp.clients.bookReview;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient("book-review")
public interface ReviewClient {
    @GetMapping("/api/book-review/avarenge-rate/{bookId}")
    Object getAverageRate(@PathVariable UUID bookId);
}
