package uz.pdp.clients.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.springframework.cloud.openfeign.FeignClient;

import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.UUID;

// Bahodir Hasanov 4/23/2022 3:44 PM

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class BookDto {
    UUID id;
    UUID photoId;
    String title;
    UUID createdBy;
    Timestamp createdAt;


}
