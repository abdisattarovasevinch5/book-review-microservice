package uz.pdp.clients.book;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

// Bahodir Hasanov 4/23/2022 3:44 PM
@FeignClient("book")
public interface BookClient {
    @GetMapping("/api/book/{bookId}")
    BookDto getBookById(@PathVariable UUID bookId);
}
