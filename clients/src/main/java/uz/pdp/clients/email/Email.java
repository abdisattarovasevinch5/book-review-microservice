package uz.pdp.clients.email;
//Sevinch Abdisattorova 04/22/2022 8:23 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class Email {
    String to;
    String receiverFullName;
    String senderFullName;
    UUID senderId;
    String bookTitle;
    UUID bookId;
    String review;
    String acceptLink;
    String rejectLink;
}
