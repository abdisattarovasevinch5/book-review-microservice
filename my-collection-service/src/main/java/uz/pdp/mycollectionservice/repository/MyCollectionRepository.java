package uz.pdp.mycollectionservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.mycollectionservice.model.MyCollection;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MyCollectionRepository extends JpaRepository<MyCollection, UUID> {

    @Query(nativeQuery = true, value = "select cast(book_id as varchar)  from my_collection \n" +
            "where user_id= :userId")
    List<UUID> getBookIdByUserId(UUID userId);


    boolean existsMyCollectionByBookIdAndUserId(UUID bookId, UUID userId);


    Optional<MyCollection> findMyCollectionByBookIdAndUserId(UUID bookId, UUID userId);



}
