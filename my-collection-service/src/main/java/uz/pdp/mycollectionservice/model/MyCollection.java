package uz.pdp.mycollectionservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.mycollectionservice.model.template.AbsEntity;

import javax.persistence.Entity;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity
public class MyCollection extends AbsEntity {

    UUID userId;
    UUID bookId;
}
