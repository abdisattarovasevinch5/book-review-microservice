package uz.pdp.mycollectionservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mycollectionservice.service.MyCollectionService;

import java.util.UUID;

@RestController
@RequestMapping("/api/my-collection")
@RequiredArgsConstructor
public class MyCollectionController {

    private final MyCollectionService myCollectionService;

    @GetMapping("/{userId}")
    public ResponseEntity<?> getMyCollections(@PathVariable UUID userId){
        return myCollectionService.getMyCollections(userId);
    }

    @PostMapping("/{userId}/{bookId}")
    public ResponseEntity<?> addMyCollection(@PathVariable UUID userId, @PathVariable UUID bookId){
        return myCollectionService.addMyCollection(userId,bookId);
    }

    @DeleteMapping("/{userId}/{bookId}")
    public ResponseEntity<?> deleteMyCollection(@PathVariable UUID userId, @PathVariable UUID bookId){
        return myCollectionService.deleteMyCollection(userId, bookId);
    }
}
