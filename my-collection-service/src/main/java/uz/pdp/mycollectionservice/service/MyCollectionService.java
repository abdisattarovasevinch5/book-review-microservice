package uz.pdp.mycollectionservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.pdp.clients.book.BookClient;
import uz.pdp.clients.book.BookDto;
import uz.pdp.mycollectionservice.model.MyCollection;
import uz.pdp.mycollectionservice.repository.MyCollectionRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MyCollectionService {

    private final MyCollectionRepository myCollectionRepository;
    private final RestTemplate restTemplate;
    private final BookClient bookClient;

    public ResponseEntity<?> getMyCollections(UUID userId) {

        List<UUID> bookIdList = myCollectionRepository.getBookIdByUserId(userId);

//        List<Object> myCollections = new ArrayList<>();
        List<BookDto> myCollections = new ArrayList<>();

        for (UUID bookId : bookIdList) {
//            Object book = restTemplate.getForObject("http://book/api/book/" + bookId, Object.class);
            BookDto book = bookClient.getBookById(bookId);
            myCollections.add(book);
        }
        return ResponseEntity.ok(myCollections);
    }

    public ResponseEntity<?> addMyCollection(UUID userId, UUID bookId) {

        if (!myCollectionRepository.existsMyCollectionByBookIdAndUserId(bookId,userId)) {
            myCollectionRepository.save(new MyCollection(userId,bookId));
            return ResponseEntity.ok("Saved !");
        }
        return ResponseEntity.badRequest().body("Uje bor bratan 😎");
    }

    @Transactional
    public ResponseEntity<?> deleteMyCollection(UUID userId, UUID bookId) {
        Optional<MyCollection> optionalMyCollection = myCollectionRepository.findMyCollectionByBookIdAndUserId(bookId, userId);
        if (optionalMyCollection.isPresent()) {
            myCollectionRepository.deleteById(optionalMyCollection.get().getId());
            return ResponseEntity.ok("Deleted !");
        }
        return ResponseEntity.badRequest().body("Book is not found !");
    }
}
