package uz.pdp.authservice.dto;
//Sevinch Abdisattorova 05/13/2022 8:23 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.PackagePrivate;
import uz.pdp.authservice.validation.PasswordsEqualConstraint;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@PasswordsEqualConstraint.List({
        @PasswordsEqualConstraint(
                field = "password",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class UserDto {

    //    @NotNull
    @Size(min = 3, message = "Name should consist of at least 3 letters")
    String fullName;
    @NotEmpty(message = "Email can't be empty")
    String email;
    //    @ValidPassword
    @NonNull
    @NotBlank(message = "New password is mandatory")
    String password;

   @NonNull
    @NotBlank(message = "Confirm Password is mandatory")
    String confirmPassword;

}
