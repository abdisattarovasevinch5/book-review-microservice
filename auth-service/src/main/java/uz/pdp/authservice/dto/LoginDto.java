package uz.pdp.authservice.dto;
//Sevinch Abdisattorova 05/20/2022 9:10 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class LoginDto {
    String email;
    String password;
}
