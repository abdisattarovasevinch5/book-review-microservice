package uz.pdp.authservice.service;
//Sevinch Abdisattorova 05/19/2022 7:24 PM

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.authservice.dto.UserDto;
import uz.pdp.authservice.model.Role;
import uz.pdp.authservice.model.RoleEnum;
import uz.pdp.authservice.model.User;
import uz.pdp.authservice.repository.RoleRepository;
import uz.pdp.authservice.repository.UserRepository;
import uz.pdp.exception.ResourceNotFoundException;
import uz.pdp.exception.UsernameAlreadyExistsException;

import java.util.HashSet;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService implements UserDetailsService {


    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Lazy
    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        return user;
    }


    public ResponseEntity<?> registerUser(UserDto userDto) {

        if (userRepository.existsByEmail(userDto.getEmail())) {
            log.info("registering userDto {}", userDto.getEmail());
            log.warn("email {} already exists.", userDto.getEmail());
            throw new UsernameAlreadyExistsException(
                    String.format("email %s already exists", userDto.getEmail()));
        }
        User user = User
                .builder()
                .email(userDto.getEmail())
                .fullName(userDto.getFullName())
                .build();

        Role role_user = roleRepository.findByRole(RoleEnum.ROLE_USER)
                .orElseThrow(() -> new ResourceNotFoundException("Role not found!!!"));

        user.setRoles(new HashSet<Role>() {{
            add(role_user);
        }});

        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        userRepository.save(user);
        return ResponseEntity.ok("User successfully registered!");

    }
}