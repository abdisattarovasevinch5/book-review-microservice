package uz.pdp.authservice.controller;
//Sevinch Abdisattorova 05/19/2022 7:33 PM

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.authservice.config.JwtProvider;
import uz.pdp.authservice.dto.LoginDto;
import uz.pdp.authservice.dto.UserDto;
import uz.pdp.authservice.model.User;
import uz.pdp.authservice.service.AuthService;
import uz.pdp.exception.BadRequestException;
import uz.pdp.exception.EmailAlreadyExistsException;
import uz.pdp.exception.UsernameAlreadyExistsException;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    private final AuthenticationManager authenticationManager;

    private final JwtProvider jwtProvider;



    @PostMapping("/register")
    public ResponseEntity<?> fillResume(@Valid @RequestBody UserDto userDto, BindingResult result) {
        try {
            return authService.registerUser(userDto);
        } catch (UsernameAlreadyExistsException e){
            FieldError fieldError = new FieldError("UserDto","email","Email is already taken!");
            result.addError(fieldError);
            throw new EmailAlreadyExistsException(e.getMessage());
        }catch (EmailAlreadyExistsException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginDto loginDto) {
        Authentication authenticate = authenticationManager.authenticate
                (new UsernamePasswordAuthenticationToken
                        (loginDto.getEmail(), loginDto.getPassword()));
        User user = (User) authenticate.getPrincipal();
        String token = jwtProvider.generateToken(user.getEmail(), user.getRoles());
        return ResponseEntity.ok("Bearer " + token);
    }
}
