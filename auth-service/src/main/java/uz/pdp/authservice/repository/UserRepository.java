package uz.pdp.authservice.repository;
//Sevinch Abdisattorova 05/19/2022 7:28 PM


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.authservice.model.User;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    User findByEmail(String email);

    boolean existsByEmail(String email);
}
