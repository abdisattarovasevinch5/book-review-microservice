package uz.pdp.authservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.authservice.model.Role;
import uz.pdp.authservice.model.RoleEnum;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    Optional<Role> findByRole(RoleEnum role);
}
