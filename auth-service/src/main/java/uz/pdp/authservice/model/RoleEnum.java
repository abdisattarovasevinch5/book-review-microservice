package uz.pdp.authservice.model;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_DIRECTOR
}
