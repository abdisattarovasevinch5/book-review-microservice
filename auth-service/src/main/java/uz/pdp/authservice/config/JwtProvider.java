package uz.pdp.authservice.config;
//Sevinch Abdisattorova 05/10/2022 9:51 AM


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import uz.pdp.authservice.model.Role;

import java.util.Date;
import java.util.Set;

@Component
public class JwtProvider {

    static long expireTime = 36_000_000;
    static String keyWord = "ThisIsKeyWord";

    public String generateToken(String phoneNumber, Set<Role> roles) {
        Date expireDate = new Date(System.currentTimeMillis() + expireTime);
        String token = Jwts.builder()
                .setSubject(phoneNumber)
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
                .claim("roles", roles)
                .signWith(SignatureAlgorithm.HS512, keyWord)
                .compact();
        return token;
    }

    public boolean ValidationToken(String token) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(keyWord)
                    .parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getPhoneNumberFromToken(String token) {
        String phoneNumber = Jwts.parser()
                .setSigningKey(keyWord)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return phoneNumber;
    }
}

