package uz.pdp.bookservice.service;//@AllArgsConstructor

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.bookservice.dto.BookDto;
import uz.pdp.bookservice.model.Attachment;
import uz.pdp.bookservice.model.AttachmentContent;
import uz.pdp.bookservice.model.Author;
import uz.pdp.bookservice.model.Book;
import uz.pdp.bookservice.repository.AttachmentContentRepository;
import uz.pdp.bookservice.repository.AttachmentRepository;
import uz.pdp.bookservice.repository.AuthorRepository;
import uz.pdp.bookservice.repository.BookRepository;
import uz.pdp.clients.bookReview.ReviewClient;

import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;
    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final AuthorRepository authorRepository;
    private final ReviewClient reviewClient;

    public ResponseEntity<?> saveBook(BookDto bookDto, MultipartFile file) {
        Attachment attachment = new Attachment(file.getContentType(), file.getSize(), file.getName());
        Attachment save = attachmentRepository.save(attachment);
        try {
            AttachmentContent attachmentContent = new AttachmentContent(attachment, file.getBytes());
            attachmentContentRepository.save(attachmentContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Set<UUID> bookAuthors = bookDto.getAuthors();
        Set<Author> authors = new HashSet<>();
        for (UUID uuid : bookAuthors) {
            Optional<Author> optionalAuthor = authorRepository.findById(uuid);
            if (optionalAuthor.isPresent())
                authors.add(optionalAuthor.get());
        }
        Book book = new Book(bookDto.getTittle(), bookDto.getDescription(), attachment, authors, bookDto.getCreatedBy());
        bookRepository.save(book);
        return ResponseEntity.ok("Successfully added!");
    }

    public ResponseEntity<?> getBookById(UUID bokId) {
        Optional<Book> optionalBook = bookRepository.findById(bokId);
        Book book;
        if (!optionalBook.isPresent())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        else
            book = optionalBook.get();
        Map<String, Object> bookById = getStringObjectMap(book);
        return ResponseEntity.ok(bookById);
    }


    public ResponseEntity<?> deleteBook(UUID bookId) {

        Optional<Book> optionalBook = bookRepository.findById(bookId);
        UUID attachmentId = optionalBook.get().getAttachment().getId();
        AttachmentContent attachmentContentByAttachmentId = attachmentContentRepository.findAttachmentContentByAttachmentId(attachmentId);
        attachmentContentRepository.deleteById(attachmentContentByAttachmentId.getId());
        attachmentRepository.deleteById(attachmentId);
        bookRepository.deleteById(optionalBook.get().getId());
        return ResponseEntity.ok("Deleted");

    }

    public ResponseEntity<?> updateBook(UUID bookId, BookDto bookDto, MultipartFile file) {
        Optional<Book> optionalBook = bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {

            Book book = optionalBook.get();
            Set<Author> authorSet = new HashSet<>();
            Set<UUID> authors = bookDto.getAuthors();
            for (UUID author : authors) {
                Optional<Author> optionalAuthor = authorRepository.findById(author);
                authorSet.add(optionalAuthor.get());
            }
            Attachment oldAttachment = optionalBook.get().getAttachment();
            UUID attachmentId = oldAttachment.getId();
            AttachmentContent attachmentContentByAttachmentId = attachmentContentRepository.findAttachmentContentByAttachmentId(attachmentId);
            attachmentContentRepository.deleteById(attachmentContentByAttachmentId.getId());
            attachmentRepository.deleteById(attachmentId);

            Attachment attachment = new Attachment(file.getContentType(), file.getSize(), file.getName());
            Attachment save = attachmentRepository.save(attachment);
            try {
                AttachmentContent attachmentContent = new AttachmentContent(save, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            book.setTittle(bookDto.getTittle());
            book.setDescription(bookDto.getDescription());
            book.setAttachment(save);
            book.setAuthor(authorSet);
            bookRepository.save(book);
            return ResponseEntity.ok("Updated");
        }
        return ResponseEntity.ok("Not found");
    }

    public ResponseEntity<?> getAllBooks() {
        List<Book> bookList = bookRepository.findAll();
        List<Object> allBook = new ArrayList<>();
        for (Book book : bookList) {
            Map<String, Object> books = getStringObjectMap(book);
            allBook.add(books);
        }
        return ResponseEntity.ok(allBook);
    }

    private Map<String, Object> getStringObjectMap(Book book) {
        Map<String, Object> bookById = new HashMap<>();
        bookById.put("id", book.getId());
        Object averageRate = reviewClient.getAverageRate(book.getId());
        bookById.put("attachmentId", book.getAttachment().getId());
        bookById.put("title", book.getTittle());
        bookById.put("description", book.getDescription());
        bookById.put("createdBy", book.getCreatedBy());
        bookById.put("authorIds", book.getAuthor());
        bookById.put("rate", averageRate);
        bookById.put("createdAt", book.getCreatedAt());
        return bookById;
    }
}
