package uz.pdp.bookservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.bookservice.model.Attachment;

import javax.persistence.OneToOne;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
//@Entity
//import static uz.sardor.Main.*;
//Sardor {4/22/2022}{ 4:37 AM}
public class BookDto {
    String tittle;
    String description;
    UUID createdBy;
    Set<UUID> authors;

    public BookDto(String tittle, String description, Set<UUID> authors) {
        this.tittle = tittle;
        this.description = description;
        this.authors = authors;
    }
}
