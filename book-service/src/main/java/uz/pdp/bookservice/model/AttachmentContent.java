package uz.pdp.bookservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.bookservice.model.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "attachment_contents")
@PackagePrivate
public class AttachmentContent extends AbsEntity {

    @OneToOne
    Attachment attachment;

    byte[] date;

}
