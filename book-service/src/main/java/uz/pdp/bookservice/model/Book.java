package uz.pdp.bookservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.bookservice.model.template.AbsEntity;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "books")
@PackagePrivate
public class Book extends AbsEntity {

    String tittle;

    String description;

    @OneToOne
    Attachment attachment;

    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable(name = "books_authors",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    Set<Author> author;

    UUID createdBy;
}
