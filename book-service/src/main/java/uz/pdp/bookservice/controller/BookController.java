package uz.pdp.bookservice.controller;//@AllArgsConstructor


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.bookservice.dto.BookDto;
import uz.pdp.bookservice.service.BookService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/book")
public class BookController {
private final BookService bookService;

    @PostMapping
    public ResponseEntity<?> addBook(@RequestPart BookDto bookDto, @RequestPart MultipartFile file) {
        return bookService.saveBook(bookDto,file);
    }

    @GetMapping
    public ResponseEntity<?> getAllBooks() {
        return bookService.getAllBooks();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable UUID id) {

        return bookService.deleteBook(id);
    }
    @GetMapping({"/{bookId}"})
    public ResponseEntity<?> getBookById(@PathVariable UUID bookId) {
        return bookService.getBookById(bookId);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable UUID id, @RequestPart BookDto bookDto,@RequestPart MultipartFile file) {
        return bookService.updateBook(id,bookDto,file);
    }



}
