package uz.pdp.bookservice.repository;//@AllArgsConstructor

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bookservice.model.Book;

import java.util.UUID;

public interface BookRepository extends JpaRepository<Book,UUID> {
}
