package uz.pdp.bookservice.repository;//@AllArgsConstructor

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookservice.model.Author;
import uz.pdp.bookservice.model.Book;

import java.util.UUID;

public interface AuthorRepository extends JpaRepository<Author,UUID> {

    @Query(nativeQuery = true,value = "select a.id,\n" +
            "       a.full_name,\n" +
            "       a.description\n" +
            "from authors a\n" +
            "         join books_authors ba on ba.author_id = a.id\n" +
            "         join books b on b.id = ba.book_id where b.id =:bookId")
    Author getAuthorByBookId(UUID bookId);

}
