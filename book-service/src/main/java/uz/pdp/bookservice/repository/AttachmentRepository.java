package uz.pdp.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bookservice.model.Attachment;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
}
