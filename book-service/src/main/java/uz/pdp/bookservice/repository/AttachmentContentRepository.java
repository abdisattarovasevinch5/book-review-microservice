package uz.pdp.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookservice.model.Attachment;
import uz.pdp.bookservice.model.AttachmentContent;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {

    boolean existsByAttachmentId(UUID attachment_id);

    AttachmentContent findAttachmentContentByAttachmentId(UUID attachment_id);
}
